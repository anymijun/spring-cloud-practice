package org.nr.tour.common.service;

import org.nr.tour.domain.Scenery;

/**
 * @author chenhaiyang <690732060@qq.com>
 */
public interface SceneryServiceDefinition extends AbstractServiceDefinition<Scenery,String> {
}
